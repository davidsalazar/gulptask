var APP = (function($, window, document, undefined) {

	var makeLinkable = function(e) {
		var $this = $(this);
		e.preventDefault();
		e.stopPropagation();
		var $link = $this.find('a');
		if ($link.length)
			if ($link.attr('target'))
				window.open($link.attr('href'), $link.attr('target'));
			else
				window.location.href = $link.attr('href');
	};
	
	$(function() {
		APP.go();
		alert('hello');
	});

	return {
		go: function() {
			var i, j = this.init;

			for (i in j) {
				j.hasOwnProperty(i) && j[i]();
			}
		},
		init: {
			els: function() {

			},

			general: function() {
				$('input, textarea').placeholder();
				$('a.scrollto').click(function(e) {
					e.preventDefault();
					var $this = $(this);
					$.scrollTo($($this.attr('href')), 800);
				});

				$('main').fitVids({ customSelector: "iframe[src^='https://maps'], iframe[src^='http://maps']"});

				$('.clickable').on('click', makeLinkable);
			},

			wcag: function() {
				$('body').append('<span hidden style="display: none;" id="new-window-0">Opens in a new window</span>');
				$('a').each(function() {
					var $this = $(this);
					if ($this.attr('target'))
						$this.attr('aria-describedby', 'new-window-0');
				});
				$('main').prop('id', 'main-content');

				$('.alert').attr('role', 'alert').attr('tabindex', '0').focus();
				$('.mm-next').attr('aria-label', 'Open Subnavigation');

			},

			header: function() {
				$('#offcanvas-nav').mmenu({
					slidingSubmenus: false
				});

				$('#offcanvas-locator').mmenu({
					slidingSubmenus: false,
					offCanvas: {
						position  : 'right'
					}
				});

				// $(window).on('resize', function() {
				// 	subNavCheck();
				// });
				
				// var subNavCheck = function() {
				// 	if ($('aside.sub-nav ul.active li').length) {
				// 		$('#header-nav-wrapper').css('padding-bottom', $('aside.sub-nav').outerHeight(true) + 'px');
				// 		$('aside.sub-nav').show();
				// 		$('main').css('padding-top', (167 + $('aside.sub-nav').outerHeight(true)) + 'px')
				// 	}
				// 	else {
				// 		$('aside.sub-nav').hide();
				// 	}
				// };

				// if (APP.util.mobileAndTabletcheck()) {
				// 	subNavCheck();
				// 	return true;
				// }

				// $('nav.header-nav li[data-page-slug]').each(function() {
				// 	var $this = $(this);
				// 	var slugId = $this.attr('data-page-slug');

				// 	$this.hover(function() {
				// 		$('aside.sub-nav ul').hide();
				// 		if ($('aside.sub-nav ul[data-page-slug=' + slugId + '] li').length)
				// 			$('aside.sub-nav').show();
				// 		else
				// 			$('aside.sub-nav').hide();

				// 		$('aside.sub-nav ul[data-page-slug=' + slugId + ']').show();
				// 		$('nav.header-nav li[data-page-slug]').removeClass('hover');
				// 		$this.addClass('hover');
				// 	}, function() {

				// 	});

				// });

				// $('#header-nav-wrapper').mouseleave(function() {
				// 	$('aside.sub-nav ul').hide();
				// 	$('aside.sub-nav ul.active').show();
				// 	$('nav.header-nav li[data-page-slug]').removeClass('hover');
				// 	if ($('aside.sub-nav ul.active li').length)
				// 		$('aside.sub-nav').show();
				// 	else
				// 		$('aside.sub-nav').hide();
				// });

				// subNavCheck();

			},

			homePage: function() {
				if ($('#promo-tiles').length == 0)
					return false;

				$('#promo-tiles .promo-tile-body').matchHeight({byRow: false});
				$('#promo-tiles .promo-tile').matchHeight({byRow: false});
				$('#promo-tiles .cta-buttons li').on('click', makeLinkable);
				$('#promo-tiles .promo-tile').on('click', makeLinkable);

				var initPromoTiles = function() {
					$('#promo-tiles .promo-tile').addClass('animated fadeIn').css('visibility', 'visible');
				};

				var $promoTileMaps = $('#promo-tiles .promo-tile-map');
				var $promoTileState = $('#promo-tiles .promo-tile-state');
				if ($promoTileMaps.length || $promoTileState.length) {

					$.get('/fetch-locations-near', function(data) {

						if (data && ('locations' in data) && data.locations.length) {
							var stateCode = data.locations[0].state;

							// if ($promoTileMaps.length) {
							// 	window.mapLocations = data.locations;
							// 	$promoTileMaps.each(function() {
							// 		var $this = $(this);

							// 		var $a = $this.find('a');
							// 		$a.attr('href', $a.attr('href') + '?q=' + stateCode);
							// 		$this.off('click');
							// 		var selector = '#' + $this.find('.promo-tile-img').attr('id');
							// 		var innerSelector = $this.find('.promo-tile-img').attr('id') + '-inner';
							// 		var prevHeight = $(selector).height();
							// 		$(selector).empty().css({'height': (prevHeight ? prevHeight : '271') + 'px', 'width': '100%'}).html('<div id="' + innerSelector + '" style="width: 100%; height: 100%;"></div>');

							// 		Maps.util.initMap('#' + innerSelector, 5);
							// 	});								
							// }

							if ($promoTileState.length && !$.isEmptyObject(data.promo_tile)) {
								var statePromoTile = data.promo_tile;
								// var statePromoTileImg = data.promo_tile_img;
								$promoTileState.each(function() {
									var $this = $(this);

									var $a = $this.find('a');
									$a.attr('href', statePromoTile.cta_link);
									if (statePromoTile.cta_name.length)
										$a.text(statePromoTile.cta_name);

									// $this.find('.promo-tile-img').html('<img src="https://dlg7f659mb7jz.cloudfront.net/files/frontend/original/' + statePromoTileImg.image + '" alt="' + statePromoTile.alt_text + '" />');
									// $this.find('.mobile-promo-tile-img').html('<img src="https://dlg7f659mb7jz.cloudfront.net/files/frontend/original/' + statePromoTileImg.mobile_image + '" alt="' + statePromoTile.alt_text + '" />');
									$this.find('h2').text(statePromoTile.title);
									$this.find('h3').text(statePromoTile.subtitle);
								});								
							}

							initPromoTiles();
						}
						else {
							initPromoTiles();
						}
					});
				} else {
					initPromoTiles();
				}

			},

			clientReviews: function() {
				if ($('#client-reviews').length == 0)
					return false;

				$('#client-reviews').slick({
					autoplay: true,
					autoplaySpeed: 6000,
					dots: false,
					arrows: true,
					prevArrow: '<button type="button" class="slick-prev" aria-label="Previous Slide for Client Reviews carousel"><span class="sr-only">Previous</span></button>',
					nextArrow: '<button type="button" class="slick-next" aria-label="Next Slide for Client Reviews carousel"><span class="sr-only">Next</span></button>'
				}).on('afterChange', function(event, slick, currentSlide, nextSlide){
  					var $focusedElement = $(document.activeElement);
  					if ($focusedElement.length && $focusedElement.hasClass('slick-arrow')) {
  						$('div.slick-active', '#client-reviews').focus();
  					}
				});

				$('#client-reviews').append('<div class="slick-stopper-wrapper"><button type="button" class="slick-stopper" aria-label="Stop Client Reviews Carousel"><i class="fa fa-pause" aria-hidden="true"></i></button></div>');
				$('.slick-stopper', '#client-reviews').on('click', function() {
					$(this).prop('disabled', true);
					$('#client-reviews').slick('slickPause');
				});

			},

			stickyFooter: function() {
				if (!window.localStorage || $('#sticky-footer').length == 0)
					return false;

				var stickyKey = $('#sticky-footer').attr('data-updated-at');
				if (localStorage.getItem(stickyKey))
					return false;

				$('#sticky-footer').css('display', 'block');
				$('#sticky-footer a#sticky-footer-close').on('click', function(e) {
					e.preventDefault();
					var $this = $(this);

					localStorage.setItem(stickyKey, true);

					// if ($this.attr('href').length)
					// 	if ($this.attr('target'))
					// 		window.open($this.attr('href'), '_blank');
					// 	else
					// 		window.location = $this.attr('href');

					$("#sticky-footer").remove();

				});
			},

			locations: function() {
				if ($('#stateMap').length) {
					$('#stateMap a.inactive').click(function() {
						return false;
					});

					$('#stateMap a.abbr').hover(function() {
						$('#stateMap a.state.' + $(this).attr('id').substring(6)).addClass('hover'); 
					}, function() {
						$('#stateMap a.state.' + $(this).attr('id').substring(6)).removeClass('hover'); 
					});
				}
			},

			careers: function() {
				if ($('#jobs_state').length == 0)
					return false;
				var $jobsStateSelector = $('#jobs_state');
				$jobsStateSelector.on('change', function() {
					var $this = $(this);
					window.location.href = $jobsStateSelector.attr('data-baseUrl') + '/state/' + $this.val();
				});

				$('#jobmap').mapSvg({
					source: '/js/vendor/mapsvg/usa.svg',
					colors: {
						background: '#fff',
						selected: '#6e9c78',
						hover: '#4A7D5A'
					},
    				cursor: "pointer",
					regions: jobmapRegions
				});

				var initSearch = function(params) {
					$.get('/careers', params, function(data) {
						$('#jobs-near-you').html(data);
					});
				};

				if (sessionStorage.getItem('lat')) {
					initSearch({'jobsNearYou': 1, 'lat': sessionStorage.getItem('lat'), 'lng': sessionStorage.getItem('lng'), 'limit': 20});
				}
				else {				
					Swal.fire({type: 'info', title: "Finding Your Location", text: "Please allow your browser to share your current location.", showConfirmButton: false});
					Maps.util.getUserLocation(function(position) {
						Swal.close();
						var lat = position.coords.latitude;
						var lng = position.coords.longitude;
						sessionStorage.setItem('lat', lat);
						sessionStorage.setItem('lng', lng);
						initSearch({'jobsNearYou': 1, 'lat': lat, 'lng': lng, 'limit': 20});
					}, function() {
					});
				}
			},

			rightColumns: function() {
				if (typeof locationRegions != 'undefined') {
					$('#locationRegions').mapSvg({
						source: '/js/vendor/mapsvg/usa.svg',
						colors: {
							background: '#fff',
							selected: '#6e9c78',
							hover: '#4A7D5A'
						},
	    				cursor: "pointer",
						regions: locationRegions
					});

					var params = {access_token: mapboxgl.accessToken, country: 'US,CA', types: 'region,postcode,place'};
					$('#store-locator-q').autocomplete({
						lookup: function (query, done) {
							$.get("https://api.mapbox.com/geocoding/v5/mapbox.places/" + encodeURIComponent(query) + ".json/?" + $.param(params), function(data) {
								var result = {
									suggestions: []
								};

								if ('features' in data) {
									$.each(data.features, function(k, v) {
										result.suggestions.push({value: v.place_name, data: v.place_name, coords: v.center});
									});

								}

								done(result);
							});
						},
						onSelect: function (suggestion) {
							var lat = suggestion.coords[1];
							var lng = suggestion.coords[0];
							var q = $('#store-locator-q').val();

							if (q == 'New York, New York, United States')
								q = 'New York, United States';
							
							window.location.href = '/massage-places-near-me?q=' + q;
						},

						autoSelectFirst: true
					});
				}
			},

			storeLocator: function() {
				if ($('#store-locator-results').length == 0)
					return false;

				var storeLocatorResults = riot.mount('store-locator-results')[0];
				var searchResultsDescription = riot.mount('search-results-description')[0];

				var pagerLimit = 5;
				var initPager = function(data) {
					var pages = Math.ceil(data.locationCount / pagerLimit);
					if (pages > 1) {
						var storeLocatorResultsPager = riot.mount('#store-locator-results-pager', {'total': pages, 'nblocks': 0})[0];
						storeLocatorResultsPager.on('pageChange', function(e) {
							// console.log('Single paginator event now do something page ' + e.page);
							var start = e.page == 1 ? 0 : ((e.page - 1) * pagerLimit);

							storeLocatorResults.set({'locations': data.locations.slice(start, start + pagerLimit), 'googleMap': data.googleMap});
							$.scrollTo('main', {duration: 300});
							if (typeof _st != 'undefined')
								_st.render();
						}).trigger('pageChange', {'page': 1});
					}
					else {
						$('#store-locator-results-pager').empty();
						storeLocatorResults.set(data);
						if (typeof _st != 'undefined')
							_st.render();
					}
				};

				var initSearch = function(params) {
					$('#state_selector').hide();
					$.getJSON('/locator', params, function(data) {
						$('#map').empty();

						window.mapLocations = data.locations;

						var i = 0;
						for (v in data.locations)
							data.locations[v]['locationIndex'] = i++;

						Maps.util.initMap();
						if (searchResultsDescription) {
							searchResultsDescription.set(data);
							$('.search-results-description').attr('tabindex', '-1').focus();
						}

						initPager(data);

					});
				};

				var autocompleteFirstRun = false;
				var params = {access_token: mapboxgl.accessToken, country: 'US,CA', types: 'region,postcode,place'};
				$('#store-locator-q').autocomplete({
					lookup: function (query, done) {
						$.get("https://api.mapbox.com/geocoding/v5/mapbox.places/" + encodeURIComponent(query) + ".json/?" + $.param(params), function(data) {
							var result = {
								suggestions: []
							};

							if ('features' in data) {
								$.each(data.features, function(k, v) {
									result.suggestions.push({value: v.place_name, data: v.place_name, coords: v.center});
								});

							}

							done(result);
						});
					},
					onSelect: function (suggestion) {
						var lat = suggestion.coords[1];
						var lng = suggestion.coords[0];
						var q = $('#store-locator-q').val();

						if (q == 'New York, New York, United States')
							q = 'New York, United States';
						
						initSearch({'q': q, 'lat': lat, 'lng': lng, 'limit': 5});
					},

					autoSelectFirst: true,

					onSearchComplete: function(query, suggestions) {
						if (autocompleteFirstRun)
							$('.autocomplete-suggestion.autocomplete-selected').click();

						autocompleteFirstRun = false;
					}
				});

				if ($('#store-locator-q').val().length) {
					$('#store-locator-q').focus();
					autocompleteFirstRun = true;
				}


				if (window.openingSoonQuery) {
					var data = {'locations': window.mapLocations, 'soon': true, 'locationCount': window.mapLocations.length};
					initPager(data);

					if (searchResultsDescription) {
						searchResultsDescription.set(data);
						$('.search-results-description').attr('tabindex', '-1').focus();
					}
					$('#map').empty();

					Maps.util.initMap();
				}

				$('#store-locator-use-my-location').on('click', function(e) {
					e.preventDefault();

					Swal.fire({type: 'info', title: "Finding Your Location", text: "Please allow your browser to share your current location.", showConfirmButton: false});
					Maps.util.getUserLocation(function(position) {
						Swal.close();
						var lat = position.coords.latitude;
						var lng = position.coords.longitude;
						initSearch({'q': '', 'lat': lat, 'lng': lng, 'limit': 5});
					}, function() {
						// swal({title: "Error!", text: "We're sorry but this feature is unsupported by your browser.", type: "error", allowOutsideClick: true});
					});
				});


				var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

				if ($('#store-locator-q').length && $('#store-locator-q').val().length)
					$('#store-locator-q').trigger("geocode");
				
				if ($('#start-geolocate').length && $('#start-geolocate').val() == 1)
					$('#store-locator-use-my-location').trigger('click');

			},

			storeServiceLocator: function() {
				if ($('#find-your-service-location').length == 0)
					return false;

				var initSearch = function(params) {
					params.serviceId = $('#serviceId').val();
					$.get('/locator', params, function(data) {
						$('#metro-locations-loader').html(data);
						$('#location-wrapper').css('display', 'block');
						if (typeof _st != 'undefined')
							_st.render();
					});
				};

				var autocompleteFirstRun = false;
				var params = {access_token: mapboxgl.accessToken, country: 'US,CA', types: 'region,postcode,place'};
				$('#store-locator-q').autocomplete({
					lookup: function (query, done) {
						$.get("https://api.mapbox.com/geocoding/v5/mapbox.places/" + encodeURIComponent(query) + ".json/?" + $.param(params), function(data) {
							var result = {
								suggestions: []
							};

							if ('features' in data) {
								$.each(data.features, function(k, v) {
									result.suggestions.push({value: v.place_name, data: v.place_name, coords: v.center});
								});

							}

							done(result);
						});
					},
					onSelect: function (suggestion) {
						var lat = suggestion.coords[1];
						var lng = suggestion.coords[0];
						var q = $('#store-locator-q').val();

						if (q == 'New York, NY, United States')
							q = 'NY, United States';
						
						initSearch({'q': q, 'lat': lat, 'lng': lng, 'limit': 5});
					},

					autoSelectFirst: true,

					onSearchComplete: function(query, suggestions) {
						if (autocompleteFirstRun)
							$('.autocomplete-suggestion.autocomplete-selected').click();

						autocompleteFirstRun = false;
					}
				});

				if ($('#store-locator-q').val().length) {
					$('#store-locator-q').focus();
					autocompleteFirstRun = true;
				}

				$('#store-locator-use-my-location').on('click', function(e) {
					e.preventDefault();

					Swal.fire({type: 'info', title: "Finding Your Location", text: "Please allow your browser to share your current location.", showConfirmButton: false});
					Maps.util.getUserLocation(function(position) {
						swal.close();
						var lat = position.coords.latitude;
						var lng = position.coords.longitude;
						initSearch({'q': '', 'lat': lat, 'lng': lng, 'limit': 5});
					}, function() {
						// swal({title: "Error!", text: "We're sorry but this feature is unsupported by your browser.", type: "error", allowOutsideClick: true});
					});
				});


				if ($('#store-locator-q').length && $('#store-locator-q').val().length)
					$('#store-locator-q').trigger("geocode");
				
				if ($('#start-geolocate').length && $('#start-geolocate').val() == 1)
					$('#store-locator-use-my-location').trigger('click');

			},

			promoBanners: function() {
				if ($('#sliding-promo-banners li').length == 0)
					return false;

				$(window).on('resize', function() {
					var windowWidth = $(window).width();
					if (windowWidth < 768) {
						$('#promo-banner-wrapper .promo-banner-right').css('margin-top', '0');
					}
					else {
						$('#promo-banner-wrapper .promo-banner-right').css('margin-top', function() {
							 var $this = $(this);
							 return (($this.parent().height() - $(this).height()) / 2) + 'px';
						});
					}
				});
				

				$(window).on('load', function() {
					$(window).resize();
				});
				
				// $('.promo-banner .matchheight').matchHeight({byRow: false});

				$('#sliding-promo-banners').bxSlider({
					adaptiveHeight: true,
					controls: false,
					pager: false,
					auto: true,
					pause: 5000,
					touchEnabled: false,
					onSliderLoad: function() {									
						$('#sliding-promo-banners-wrapper').css('visibility', 'visible');
					}
				});


			},

			forms: function() {
				$('form .submit-button').on('mouseover', function() {
					$('#name2').val('form-submitted');
				}).on('click', function() {
					var $this = $(this);
					$this.parents('form').submit();
				});

				$('.form-wrapper').each(function() {
					var $this = $(this);
					if ($this.attr('data-post-request') && $this.attr('data-post-request') == 1) {
						$.scrollTo($this, 800);
					}
				});

				$('form.submit-once').on('submit', function(e) {
					var $this = $(this);
					if ($this.data('form-submitted'))
					{
						return false;
					}
					$this.data('form-submitted', true);
				});

			}
		},
		util: {
			mobileAndTabletcheck: function() {
			  var check = false;
			  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
			  return check;
			}
		}
	};

})(jQuery, this, this.document);


/*global jQuery */
/*jshint browser:true */
/*!
* FitVids 1.1
*
* Copyright 2013, Chris Coyier - http://css-tricks.com + Dave Rupert - http://daverupert.com
* Credit to Thierry Koblentz - http://www.alistapart.com/articles/creating-intrinsic-ratios-for-video/
* Released under the WTFPL license - http://sam.zoy.org/wtfpl/
*
*/

(function( $ ){

  "use strict";

  $.fn.fitVids = function( options ) {
    var settings = {
      customSelector: null
    };

    if(!document.getElementById('fit-vids-style')) {
      // appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
      var head = document.head || document.getElementsByTagName('head')[0];
      var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
      var div = document.createElement('div');
      div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
      head.appendChild(div.childNodes[1]);
    }

    if ( options ) {
      $.extend( settings, options );
    }

    return this.each(function(){
      var selectors = [
        "iframe[src*='player.vimeo.com']",
        "iframe[src*='youtube.com']",
        "iframe[src*='youtube-nocookie.com']",
        "iframe[src*='kickstarter.com'][src*='video.html']",
        "object",
        "embed"
      ];

      if (settings.customSelector) {
        selectors.push(settings.customSelector);
      }

      var $allVideos = $(this).find(selectors.join(','));
      $allVideos = $allVideos.not("object object"); // SwfObj conflict patch

      $allVideos.each(function(){
        var $this = $(this);
        if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
        var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
            width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
            aspectRatio = height / width;
        if(!$this.attr('id')){
          var videoID = 'fitvid' + Math.floor(Math.random()*999999);
          $this.attr('id', videoID);
        }
        $this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+"%");
        $this.removeAttr('height').removeAttr('width');
      });
    });
  };
// Works with either jQuery or Zepto
})( window.jQuery || window.Zepto );
