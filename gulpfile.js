// Include gulp
const { src, dest, watch, series, parallel } = require('gulp');

// Include Our Plugins
const jshint = require('gulp-jshint');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const header = require('gulp-header');
const minifycss = require('gulp-clean-css');
const rebase = require('gulp-css-rebase');

var pkg = require('./package.json');
var banner = [''].join('\n');

var buildFiles = {
    corp: {
        buildpath: {
            'js': 'js/min',
            'css': 'css/min'
        },
        'js': [
            'one/*.js',
            'two/*.js'

        ],
        'css': [
        ]
    }
};


// Compile Our Sass
// gulp.task('sass', function() {
//     return gulp.src('scss/*.scss')
//         .pipe(sass())
//         .pipe(gulp.dest('css'));
// });

// Concatenate & Minify JS
function javascript(cb) {

    for (var k in buildFiles)
    {
        var files = buildFiles[k];
        src(files.js)
            .pipe(concat('all.js'))
            .pipe(dest(files.buildpath.js))
            .pipe(rename('all.min.js'))
            .pipe(uglify())
            .pipe(header(banner, { pkg : pkg } ))
            .pipe(dest(files.buildpath.js));        
    }

    cb();
}

function css(cb) {
    for (var k in buildFiles)
    {
        var files = buildFiles[k];
        src(files.css)
            .pipe(rebase({output_css: files.buildpath.css, output_assets: files.buildpath.css}))
            .pipe(concat('all.css'))
            .pipe(dest(files.buildpath.css))
            .pipe(rename('all.min.css'))
            .pipe(minifycss({specialComments: 0}))
            .pipe(header(banner, {pkg : pkg}))
            .pipe(dest(files.buildpath.css));
    }

    cb();
}



function watcher() {

    for (var k in buildFiles) {
        var files = buildFiles[k];

        watch(files.js, { ignoreInitial: false }, javascript);
        watch(files.css, { ignoreInitial: false }, css);
    } 

}

exports.default = function() {
    watcher();
};